/**
 * Created by Admin on 12.10.2016.
 */
$(document).ready(function(){
    var text_slides=$(".slider-description");
    var $main_slider = new Swiper('.main-slider', {
        slidesPerView: 1,
        speed: 2000,
        autoplay: 3000,
        effect: 'fade',
        spaceBetween: 30,
        pagination: ".swiper-pagination",
        paginationClickable: "true",
        onSlideChangeStart: function(swiper){
            text_slides.fadeOut(500);
            setTimeout(function () {
                text_slides.eq($main_slider.activeIndex).fadeIn(500);
                text_slides.removeClass("animate-description");
            },600);
        }
    });
    var $comment_slider = new Swiper('.c-events-comments-slider', {
        slidesPerView: 3,
        speed: 2000,
        effect: 'slide',
        loop: "true",
        spaceBetween: 20,
        prevButton: ".c-events-comments-arrow-left",
        nextButton: ".c-events-comments-arrow-right",
        breakpoints: {
            1600: {
                slidesPerView: 2,
                spaceBetween: 20
            }
        }
    });
});
