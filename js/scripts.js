/**
 * Created by Admin on 04.10.2016.
 */
$(document).ready(function(){

    
    var $numbers=$(".counter-number"),
    $numbers_count=$(".counter-number").length,
    $number,
    q=0;
    if($numbers_count){
        setTimeout(function () {
            movenumber($numbers.eq($numbers_count-1),$numbers_count);
        },2150);
    }
    function movenumber($cur_number,quantity){
        $number=$cur_number.find(".counter-number-old").text();
        $cur_number.find(".counter-number-old").animate({bottom: "-100%"},1500);
        $cur_number.find(".counter-number-new").animate({bottom: "0"},1500);
        if($number==9){
            q++;
            var z=--quantity;

            if(z>0){
                setTimeout(function () {
                    movenumber($numbers.eq($numbers_count-1-q),z);
                },500);
            }
        }
    }
    // $(".main-slider-img-holder").each(function () {
    //     var img_url=$(this).find("img").attr("src");
    //    $(this).css("background","url(\""+img_url+"\") no-repeat center");
    //     $(this).find("img").remove();
    // });
    var imgw=$(".main-slider-img-holder").find("img").outerWidth(),
    slw=$(window).outerWidth(),
    wdif=(imgw-slw)/2,
    resizeflag=true;

    $(".main-slider-img-holder").css("margin-left",-wdif+"px");
    $(window).resize(function () {
        if(resizeflag==true){
            resizeflag=false;
            var imgw=$(".main-slider-img-holder").find("img").outerWidth(),
                slw=$(window).outerWidth(),
                wdif=(imgw-slw)/2;

            $(".main-slider-img-holder").css("margin-left",-wdif+"px");
        }
        setTimeout(function () {
            resizeflag=true;
        },500)
    });
    $(".timeline-mask").animate({width:0},2500);
    var $timeline_points=$(".timeline-block");
    setTimeout(function () {
        timelineAppear(0);
    },1000);
    function timelineAppear(i){
        if(i<$timeline_points.length){
            $timeline_points.eq(i).fadeIn();
            setTimeout(function () {
                timelineAppear(i+1);
            },250)
        }
    }
    // openTimelineBlock($timeline_points,0);
    // function openTimelineBlock($timeline_points,timeline_number){
    //     console.log(timeline_number);
    //     $cur_timeline_block=$timeline_points.index(timeline_number);
    //     console.log($cur_timeline_block);
    //     $cur_timeline_block.animate({
    //         width: "11px",
    //         height: "11px",
    //         top: "-5px"
    //     },400);
    //     if($cur_timeline_block.hasClass("timeline-block-transparent")){
    //         $cur_timeline_block.find(".timeline-dot").animate({
    //             top:"4px",
    //             left:"3px"
    //         },400)
    //     } else {
    //         $cur_timeline_block.find(".timeline-dot").animate({
    //             top:"3px",
    //             left:"3px"
    //         },400)
    //     }
    //     if($cur_timeline_block.hasClass("timeline-block-ukraine")){
    //         setTimeout(function () {
    //             $cur_timeline_block.find(".timeline-line").animate({
    //                 height: "4.166em"
    //             },400);
    //             $cur_timeline_block.find(".timeline-infoblock").fadeIn();
    //         },400);
    //     }
    //     if($cur_timeline_block.hasClass("timeline-block-empty")){
    //         setTimeout(function () {
    //             $cur_timeline_block.find(".timeline-line").animate({
    //                 height: "5.555em"
    //             },400);
    //
    //         },400);
    //     }
    //     if($cur_timeline_block.hasClass("timeline-block-mic")){
    //         setTimeout(function () {
    //             $cur_timeline_block.find(".timeline-infoblock").fadeIn();
    //         },400)
    //     }
    //     if($cur_timeline_block.hasClass("timeline-block-europe")){
    //         setTimeout(function () {
    //             $cur_timeline_block.find(".timeline-line").animate({
    //                 height: "2.777em"
    //             });
    //             setTimeout(function () {
    //                 $cur_timeline_block.find(".timeline-infoblock").fadeIn();
    //             },400);
    //         },400)
    //     }
    //     if($cur_timeline_block.hasClass(".timeline-block-counter")){
    //         setTimeout(function () {
    //             $cur_timeline_block.find(".timeline-line").animate({
    //                 height: "4.166em"
    //             });
    //             setTimeout(function () {
    //                 $cur_timeline_block.find(".timeline-infoblock").fadeIn();
    //             },400);
    //         },400)
    //     }
    //     if($cur_timeline_block.hasClass("timeline-block-world")){
    //         setTimeout(function () {
    //             $cur_timeline_block.find(".timeline-line").animate({
    //                 height: "2.777em"
    //             },400);
    //             setTimeout(function () {
    //                 $cur_timeline_block.find(".timeline-infoblock").fadeIn();
    //             },400);
    //         },400);
    //     }
    //     if(timeline_number<$timeline_points.length-1){
    //         setTimeout(openTimelineBlock($timeline_points,timeline_number+1),400);
    //     }
    //
    // }
    var $wh=$(window).outerHeight();
    var $scroll_obj=$(".scrollSensitive");
    for(var i=0;i<$scroll_obj.length;i++){
        if($scroll_obj[i].getBoundingClientRect().top<$wh*0.75){
            $scroll_obj.eq(i).addClass("scrollTriggered");
        }
    }
    $(document).scroll(function () {
        for(var i=0;i<$scroll_obj.length;i++){
            if($scroll_obj[i].getBoundingClientRect().top<$wh*0.75){
                $scroll_obj.eq(i).addClass("scrollTriggered");
            }
        }

    });
});